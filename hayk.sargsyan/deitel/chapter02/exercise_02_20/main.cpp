#include <iostream>

int
main()
{
    int radius;

    std::cout << "Please enter radius of your circle: ";
    std::cin >> radius;

    std::cout << "Diameter of your circle is " << radius * 2 << "\n";
    std::cout << "Circumference of your circle is " << 3.14159 * radius * 2 << "\n";
    std::cout << "Area of your circle is " << 3.14159 * radius * radius << "\n";

    return 0;
}